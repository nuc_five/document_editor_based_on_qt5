#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPlainTextEdit>
#include <QMessageBox>
#include <QString>
#include <QFont>
#include "findDialog.h"
#include <QSharedPointer>

class MainWindow : public QMainWindow
{
    Q_OBJECT
 private:
    QPlainTextEdit* textEdit;
    QString mFilePath;
    bool isTextChanged;
    QSharedPointer<findDialog> m_pfindDialog;
    // main window
    bool creatUi();
    // menubar
    bool createMenuBar();
    bool createFileMenu(QMenuBar* mb);
    bool createEditMenu(QMenuBar* mb);
    bool createFormatMenu(QMenuBar* mb);
    bool createVeiwMenu(QMenuBar* mb);
    bool createHelpMenu(QMenuBar* mb);
    bool createAction(QAction* &action, QString text, int key);
    bool createAction(QAction* &action, QString text, QString path);
    //centraWidget
    bool createCentraWidget();
    // tool bar
    bool createToolBar();
    // status bar
    bool createStatusBar();
    void showMessageBox(QString message);
    // find dialog
    bool createFindDialog();
private slots:
    //file
    void actionNew();
    void actionOpen();
    void actionSave();
    void actionSaveAs();
    void actionExit();
    //edit
    void actionUndo();
    void actionRedo();
    void actionCpy();
    void actionPast();
    void actionFind();
    //format
    void actionFont();
    // veiw
    void actionStatusBar();
    // help
    void actionHelp();
    // 文档中是否有未保存的数据槽函数
    void currentTextChanged();
    //设置选中字体格式
    void setInsertFont(const QFont &font);
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
     void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
