#include "mainwindow.h"
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QWidget>
#include <QHBoxLayout>
#include <QToolBar>
#include <QStatusBar>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), textEdit( new QPlainTextEdit()), mFilePath(" "), isTextChanged(false),
      m_pfindDialog(new findDialog(this, textEdit))
{
    creatUi();
    qDebug() << textEdit;
}
//2018-05-22
bool MainWindow::creatUi() {
   bool ret = true;
   this->setWindowTitle("HZ Note [ ]");
   ret = ret && createMenuBar();
   ret = ret && createToolBar();
   ret = ret && createCentraWidget();
   ret = ret && createStatusBar();
   return ret;
}

bool MainWindow::createMenuBar() {
    QMenuBar *mb = this->menuBar();
    bool ret = (mb != NULL);
    if(ret) {
        mb->setNativeMenuBar(false);//非mac上不用这句话
        ret = ret && createFileMenu(mb);
        ret = ret && createEditMenu(mb);
        ret = ret && createFormatMenu(mb);
        ret = ret && createVeiwMenu(mb);
        ret = ret && createHelpMenu(mb);
    }
    return ret;
}

bool MainWindow::createFileMenu(QMenuBar* mb) {
    QMenu *menu = new QMenu("File(&F)");

    bool ret = (menu != NULL);

    if(ret) {
        QAction* action =  NULL;
        ret = ret && createAction(action, "New",  Qt::CTRL + Qt::Key_N);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionNew()));
        }

        ret = ret && createAction(action, "Open",  Qt::CTRL + Qt::Key_O);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionOpen()));
        }

        ret = ret && createAction(action, "Save",  Qt::CTRL + Qt::Key_S);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionSave()));
        }

        ret = ret && createAction(action, "Save as..", 0);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionSaveAs()));
        }

        menu->addSeparator();

        ret = ret && createAction(action, "Exit", Qt::Key_X);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionExit()));
        }
    }

    if(ret) {
        mb->addMenu(menu);
    } else {
        delete menu;
        ret = false;
    }

    return ret;
}

bool MainWindow::createEditMenu(QMenuBar* mb) {
    QMenu* menu =  new QMenu("Edit(&E)");

    bool ret = (menu != NULL);

    if(ret) {
        QAction* action =  NULL;

        ret = ret && createAction(action, "Undo", Qt::CTRL + Qt::Key_Z);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)),  this,  SLOT(actionUndo()));
        }

        ret = ret && createAction(action, "Redo", Qt::CTRL + Qt::Key_X);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)),  this,  SLOT(actionRedo()));
        }

        ret = ret && createAction(action, "Copy", Qt::CTRL + Qt::Key_C);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)),  this,  SLOT(actionCpy()));
        }

        ret = ret && createAction(action, "Past", Qt::CTRL + Qt::Key_V);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)),  this,  SLOT(actionPast()));
        }

        ret = ret && createAction(action, "Find", Qt::CTRL + Qt::Key_Z);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)),  this,  SLOT(actionFind()));
        }
    }

    if(ret) {
        mb->addMenu(menu);
    }else {
      delete menu;
      ret = false;
    }

    return ret;
}

bool MainWindow::createFormatMenu(QMenuBar* mb) {
    QMenu* menu = new QMenu("Format");
    bool ret = (menu != NULL);
    if(ret) {
        QAction* action = NULL;
        ret = ret && createAction(action, "Format", Qt::CTRL + Qt::Key_F);
        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionFont()));
        }
    }
    if(ret) {
        mb->addMenu(menu);
    } else {
        delete menu;
        ret = false;
    }
    return ret;
}
//
bool MainWindow::createVeiwMenu(QMenuBar* mb) {
    QMenu* menu = new QMenu("View(&V)");
    bool ret = (menu != NULL);
    if(ret) {
        QAction* action = NULL;
        ret = ret && createAction(action, "Status", Qt::Key_S);

        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionStatusBar()));
        }
    }

    if(ret) {
        mb->addMenu(menu);
    } else {
        delete menu;
        ret = false;
    }
    return ret;
}
//help
bool MainWindow::createHelpMenu(QMenuBar* mb) {
    QMenu* menu = new QMenu("Help(&H)");
    bool ret = (menu != NULL);
    if(ret) {
        QAction* action = NULL;
        ret = ret && createAction(action, "关于", Qt::Key_A);

        if(ret) {
            menu->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionHelp()));
        }
    }

    if(ret) {
        mb->addMenu(menu);
    } else {
        delete menu;
    }
    return ret;
}
// menu bar overload
bool MainWindow::createAction(QAction* &action, QString text, int key) {
    bool ret = true;
    action = new QAction(text, NULL);//add action auto set parents
    ret = (action != NULL);
    if(ret) {
        action->setShortcut(QKeySequence(key));
    }
    return ret;
}
// tool bar overload
bool MainWindow::createAction(QAction* &action, QString text, QString path) {
    bool ret = true;
    action = new QAction(" ", NULL);//add action auto set parents
    if(action != NULL) {
        action->setToolTip(text); // set tool tip
        action->setIcon(QIcon(path));
    } else {
        ret = false;
        delete action;
    }
    return ret;
}
// centra widget
bool MainWindow::createCentraWidget() {
    QWidget* widget =  new QWidget(this);
    bool ret = (widget != NULL);
    if(ret) {
        this->setCentralWidget(widget);
        QHBoxLayout* hlayout = new QHBoxLayout(widget);
        textEdit = new QPlainTextEdit(widget);
        QObject::connect(textEdit, SIGNAL(textChanged()), this, SLOT(currentTextChanged()));
        ret = ret && (hlayout != NULL) && (textEdit != NULL);
        if(ret) {
            hlayout->addWidget(textEdit);
            widget->setLayout(hlayout);
        } else {
            delete widget;
            ret = false;
        }
    }
    return ret;
}
// create tool bar
bool MainWindow::createToolBar() {
    QToolBar* tb = this->addToolBar("tool bar");
    bool ret = (tb != NULL);
    if(ret) {
        tb->setIconSize(QSize(16, 16));
        tb->setFloatable(false); // float
        tb->setMovable(false); // move
        QAction* action = NULL;
        ret = ret && createAction(action, "Open", ":/toolbarIcon/pic/open.png");
        if(ret) {
            tb->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionOpen()));
        }

        ret = ret && createAction(action, "new", ":/toolbarIcon/pic/new.png");
        if(ret) {
            tb->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionNew()));
        }

        ret = ret && createAction(action, "Save", ":/toolbarIcon/pic/save.png");
        if(ret) {
            tb->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionSave()));
        }

        ret = ret && createAction(action, "Save As", ":/toolbarIcon/pic/saveas.png");
        if(ret) {
            tb->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionSaveAs()));
        }

        ret = ret && createAction(action, "Redo", ":/toolbarIcon/pic/redo.png");
        if(ret) {
            tb->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionRedo()));
        }

        ret = ret && createAction(action, "Undo", ":/toolbarIcon/pic/undo.png");
        if(ret) {
            tb->addAction(action);
            QObject::connect(action, SIGNAL(triggered(bool)), this, SLOT(actionUndo()));
        }
    }
    return ret;
}
//status bar 2018-05-2310:25:10
bool MainWindow::createStatusBar() {
    QStatusBar* sb = new QStatusBar(this);
    bool ret = (sb != NULL);
    if(ret) {
        this->setStatusBar(sb);//  deal sb in this
    }
    return ret;
}
// find dialog
bool MainWindow::createFindDialog() {
    findDialog* fDlg = new findDialog(this, textEdit);
    bool ret = (fDlg != NULL);
    if(ret) {
        fDlg->setWindowTitle("Find Dialog");
        fDlg->setAttribute(Qt::WA_DeleteOnClose);
        fDlg->setFixedSize(300, 100);
        fDlg->show();
    }
    return ret;
}


MainWindow::~MainWindow()
{

}
