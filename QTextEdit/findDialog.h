#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QDialog>
#include <QPlainTextEdit>
#include <QPointer>

namespace Ui {
class findDialog;
}

class findDialog : public QDialog
{
    Q_OBJECT

public:
    explicit findDialog(QWidget *parent = 0, QPlainTextEdit* pText = 0);
    ~findDialog();

private slots:
    void on_findBtn_clicked();

    void on_closeBtn_clicked();

private:
    Ui::findDialog *ui;
    QPointer<QPlainTextEdit> mEdit;
    void setQPlainTextEdit(QPlainTextEdit *pText);
    QPlainTextEdit* getQPlainTextEdit();
};

#endif // FINDDIALOG_H
