#include "mainwindow.h"
#include <QDebug>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include <QIODevice>
#include <QMessageBox>
#include <QTextStream>
#include <QFontDialog>
#include <QFont>
#include <QTextCursor>
#include <QTextCharFormat>
#include <QCloseEvent>

void MainWindow::showMessageBox(QString message) {
    QMessageBox msg(this);
    msg.setWindowTitle("Erro !");
    msg.setText(message);
    msg.setIcon(QMessageBox::Critical);
    msg.setStandardButtons(QMessageBox::Ok);
    msg.exec();
    return;
}
//file
void MainWindow::actionNew() {
    qDebug() << "New";
    if(isTextChanged) {
        int temp = QMessageBox::question(this,
                                         "question Dialog",
                                         "do you want to save ?",
                                          QMessageBox::Yes
                                         | QMessageBox::No
                                         | QMessageBox::Cancel);
        switch(temp) {
            case QMessageBox::Yes : {
                qDebug() << "Yes";
                QString path = QFileDialog::getSaveFileName(this, "Save File", "/", "txt (*.txt) All(*)");
                if(path != " ") {
                    QFile file(path);
                    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                        qDebug() << path;
                        QTextStream out(&file);
                        out << textEdit->toPlainText();
                        file.close();
                        this->setWindowTitle(QString("HZ Note New"));
                        textEdit->clear();
                        isTextChanged = false;
                        mFilePath = " ";
                    }
                }
                break;
            }
        case QMessageBox::No : {
                qDebug() << "No";
                textEdit->clear();
                isTextChanged = false;
                mFilePath = " ";
                break;
            }
        case QMessageBox::Cancel : {
                qDebug() << "Cancel";
                break;
            }
        }
    } else {
        textEdit->clear();
        isTextChanged = false;
        mFilePath = " ";
    }
    return ;
}

void MainWindow::actionOpen() {
    qDebug() << "Open";
    //该静态成员函数打开的路径需要有对应的文件
    QString path = QFileDialog::getOpenFileName(this, "Open File", "/",  "txt (*.txt) All (*)");
    QFile file(path);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        textEdit->setPlainText(file.readAll());
        mFilePath = path;
        file.close();
        this->setWindowTitle(QString("HZ Note - ") + "[" + mFilePath + "]");
        isTextChanged = false;
    } else {
        showMessageBox(QString("Open Failed !") + "[" + mFilePath + "]");
        mFilePath = " ";
        isTextChanged = false;
    }
    return ;
}

void MainWindow::actionSave() {
    qDebug() << "Save";
    if(mFilePath == " ") {
        mFilePath =  QFileDialog::getSaveFileName(this, "Save File", "/", "txt (*.txt);; All (*)");
    }

    if(mFilePath != " ") {
        QFile file(mFilePath);
        if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            qDebug() << mFilePath;
            QTextStream out(&file);
            out << textEdit->toPlainText();
            file.close();
            this->setWindowTitle(QString("HZ Note - [ ") + mFilePath + " ]");
            isTextChanged = false;
        } else {
            showMessageBox(QString("Save Failed ! \n\n")  + "\"" + mFilePath + "\"");
            mFilePath = " ";
        }
    }
    return ;
}

void MainWindow::actionSaveAs() {
    qDebug() << "SaveAs";
    QString path = QFileDialog::getSaveFileName(this, "Save as..", "/", "txt (*.txt);; All (*)");
    if(path != " ") {
        QFile file(path);
        if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&file);
            out << textEdit->toPlainText();
            file.close();
            mFilePath =  path;
            this->setWindowTitle(QString("HZ Note - [") + "[" + mFilePath + "]");
            isTextChanged = false;
        } else {
            showMessageBox(QString("Save As Failed ! \n\n")  + "\"" + mFilePath + "\"");
        }
    }
    return ;
}

void MainWindow::actionExit() {
    qDebug() << "Exit";
    this->close();
    return ;
}
// edit
void MainWindow::actionUndo() {
    qDebug() << "undo";
    textEdit->undo();
    return ;
}
void MainWindow::actionRedo() {
    qDebug() << "redo";
    textEdit->redo();
    return ;
}

void MainWindow::actionCpy() {
    qDebug() << "cpy";
    textEdit->copy();
    return ;
}

void MainWindow::actionPast() {
    qDebug() << "past";
    textEdit->paste();
    return ;
}

void MainWindow::actionFind() {
    qDebug() << "Find";
    bool ret = createFindDialog();
    return ;
}

// format
void MainWindow::actionFont() {
    qDebug() << "Fornt";
    bool ok;
     QFont font = QFontDialog::getFont(&ok, QFont("Helvetica [Cronyx]", 10), this);
     if (ok) {
         // the user clicked OK and font is set to the font the user selected
         this->setInsertFont(font);
     } else {
         // the user canceled the dialog; font is set to the initial
         // value, in this case Helvetica [Cronyx], 10
     }

    return ;
}
// View
void MainWindow::actionStatusBar() {
    qDebug() << "StatusBar";


    return ;
}
// help
void MainWindow::actionHelp() {
    qDebug() << "Help";
    QMessageBox::about(this, QString("About this GUI"),  QString("开发：wrf \n版本：V0.0"));
    return ;
}
// is changed
void MainWindow::currentTextChanged() {
    qDebug() << "true";
    if(!isTextChanged) {
         this->setWindowTitle(this->windowTitle() + "*");
    }
    isTextChanged = true;
}
// font
void  MainWindow::setInsertFont(const QFont &font) {
        QTextCharFormat fmt;//文本字符格式
        fmt.setFont(font);//字体
        QTextCursor cursor = textEdit->textCursor();//获取文本光标
        cursor.mergeCharFormat(fmt);//光标后的文字就用该格式显示
        textEdit->mergeCurrentCharFormat(fmt);//textEdit使用当前的字符格式
        return ;
}

 void MainWindow::closeEvent(QCloseEvent *event) {
     if(isTextChanged) {
         int temp = QMessageBox::question(this,
                                          "question Dialog",
                                          "do you want to save ?",
                                           QMessageBox::Yes
                                          | QMessageBox::No
                                          | QMessageBox::Cancel);
         switch(temp) {
             case QMessageBox::Yes : {
                // qDebug() << "Yes";
                 QString path = QFileDialog::getSaveFileName(this, "Save File", "/", "txt (*.txt) All(*)");
                 if(path != " ") {
                     QFile file(path);
                     if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                         //qDebug() << path;
                         QTextStream out(&file);
                         out << textEdit->toPlainText();
                         file.close();
                         this->setWindowTitle(QString("HZ Note New"));
                         textEdit->clear();
                         isTextChanged = false;
                         mFilePath = " ";
                     }
                 event->accept();
                 }
                 break;
             }
         case QMessageBox::No : {
               this->close();
               break;
             }
         case QMessageBox::Cancel : {
                 qDebug() << "Cancel";
                 event->ignore();
                 break;
             }
         }
     } else {
         QWidget::closeEvent(event);
     }
     return ;
}


