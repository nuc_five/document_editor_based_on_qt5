#-------------------------------------------------
#
# Project created by QtCreator 2018-05-22T10:26:45
#
#-------------------------------------------------

QT       += core gui
CONFIG  += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTextEdit
TEMPLATE = app


SOURCES += main.cpp\
    mainwindowslot.cpp \
    mainwindowUi.cpp \
    findDialog.cpp

HEADERS  += mainwindow.h \
    findDialog.h

RESOURCES += \
    res.qrc

FORMS += \
    findDialog.ui
